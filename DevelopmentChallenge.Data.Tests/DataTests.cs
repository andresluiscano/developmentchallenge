﻿using System;
using System.Collections.Generic;
using DevelopmentChallenge.Data.Classes;
using NUnit.Framework;

namespace DevelopmentChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVaciaFormasEnCastellano()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), Idioma.Castellano));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), Idioma.Ingles));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnItaliano()
        {
            Assert.AreEqual("<h1>Lista vuota di forme!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), Idioma.Italiano));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> { new FormaGeometrica(Forma.Cuadrado, 5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrados | Área 25 | Perímetro 20 <br/>TOTAL:<br/>1 formas Perímetro 20 Área 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnCuadradoEnIngles()
        {
            var cuadrados = new List<FormaGeometrica> { new FormaGeometrica(Forma.Cuadrado, 5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>1 Squares | Area 25 | Perimeter 20 <br/>TOTAL:<br/>1 shapes Perimeter 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnCuadradoEnItaliano()
        {
            var cuadrados = new List<FormaGeometrica> { new FormaGeometrica(Forma.Cuadrado, 5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.Italiano);

            Assert.AreEqual("<h1>Rapporto delle forme</h1>1 Quadrati | Area 25 | Perimetro 20 <br/>TOTALE:<br/>1 forme Perimetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                new FormaGeometrica(Forma.Cuadrado, 5),
                new FormaGeometrica(Forma.Cuadrado, 1),
                new FormaGeometrica(Forma.Cuadrado, 3)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<FormaGeometrica>
            {
                new FormaGeometrica(Forma.Cuadrado, 5),
                new FormaGeometrica(Forma.Circulo, 3),
                new FormaGeometrica(Forma.TrianguloEquilatero, 4),
                new FormaGeometrica(Forma.Cuadrado, 2),
                new FormaGeometrica(Forma.TrianguloEquilatero, 9),
                new FormaGeometrica(Forma.Circulo, 2.75m),
                new FormaGeometrica(Forma.TrianguloEquilatero, 4.2m),
                new FormaGeometrica(Forma.Trapecio, 2,2,2)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Idioma.Ingles);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13.01 | Perimeter 18.06 <br/>3 Triangles | Area 49.64 | Perimeter 51.6 <br/>1 Trapezoids | Area 4 | Perimeter 4 <br/>TOTAL:<br/>8 shapes Perimeter 101.66 Area 95.65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                new FormaGeometrica(Forma.Cuadrado, 5),
                new FormaGeometrica(Forma.Circulo, 3),
                new FormaGeometrica(Forma.TrianguloEquilatero, 4),
                new FormaGeometrica(Forma.Cuadrado, 2),
                new FormaGeometrica(Forma.TrianguloEquilatero, 9),
                new FormaGeometrica(Forma.Circulo, 2.75m),
                new FormaGeometrica(Forma.TrianguloEquilatero, 4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Idioma.Castellano);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Área 29 | Perímetro 28 <br/>2 Círculos | Área 13.01 | Perímetro 18.06 <br/>3 Triángulos | Área 49.64 | Perímetro 51.6 <br/>TOTAL:<br/>7 formas Perímetro 97.66 Área 91.65",
                resumen);
        }
    }
}
