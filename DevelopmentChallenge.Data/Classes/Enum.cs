﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentChallenge.Data.Classes
{
    #region Formas
    public enum Forma
    {
        Cuadrado = 1,
        TrianguloEquilatero = 2,
        Circulo = 3,
        Trapecio = 4
    }

    #endregion

    #region Idiomas
    public enum Idioma
    {
        Castellano = 1,
        Ingles = 2,
        Italiano = 3
    }

    #endregion
}
