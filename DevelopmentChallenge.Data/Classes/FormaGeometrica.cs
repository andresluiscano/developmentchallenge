﻿/******************************************************************************************************************/
/******* ¿Qué pasa si debemos soportar un nuevo idioma para los reportes, o agregar más formas geométricas? *******/
/******************************************************************************************************************/

/*
 * TODO: 
 * Refactorizar la clase para respetar principios de la programación orientada a objetos.
 * Implementar la forma Trapecio/Rectangulo. 
 * Agregar el idioma Italiano (o el deseado) al reporte.
 * Se agradece la inclusión de nuevos tests unitarios para validar el comportamiento de la nueva funcionalidad agregada (los tests deben pasar correctamente al entregar la solución, incluso los actuales.)
 * Una vez finalizado, hay que subir el código a un repo GIT y ofrecernos la URL para que podamos utilizar la nueva versión :).
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DevelopmentChallenge.Data.Classes
{
    public class FormaGeometrica
    {
        #region Atributos
        private readonly decimal _lado;
        private readonly decimal _altura;
        private readonly decimal _baseMayor;
        private readonly decimal _baseMenor;
        public Forma Tipo { get; set; }

        private static Dictionary<Idioma, Mensaje> mensajes = 
            new Dictionary<Idioma, Mensaje>{
                {   
                    Idioma.Castellano, 
                    new Mensaje() { 
                        ListaVacia = "<h1>Lista vacía de formas!</h1>",
                        TituloReporte = "<h1>Reporte de Formas</h1>",
                        nombresForma = new Dictionary<Forma, string>
                                        {
                                            { Forma.Cuadrado, "Cuadrados" },
                                            { Forma.Circulo, "Círculos"},
                                            { Forma.TrianguloEquilatero, "Triángulos"},
                                            { Forma.Trapecio, "Trapecios" }
                                        },
                        sTotal =  "TOTAL:<br/>",
                        sForma = "formas",
                        sPerimetro = "Perímetro",
                        sArea = "Área",
                        sDesconocido = "Forma desconocida"
                    }
                },
                { 
                    Idioma.Ingles, 
                    new Mensaje() {
                        ListaVacia = "<h1>Empty list of shapes!</h1>",
                        TituloReporte = "<h1>Shapes report</h1>",
                        nombresForma = new Dictionary<Forma, string>
                                        {
                                            { Forma.Cuadrado, "Squares" },
                                            { Forma.Circulo, "Circles"},
                                            { Forma.TrianguloEquilatero, "Triangles"},
                                            { Forma.Trapecio, "Trapezoids" }
                                        },
                        sTotal =  "TOTAL:<br/>",
                        sForma = "shapes",
                        sPerimetro = "Perimeter",
                        sArea = "Area",
                        sDesconocido = "Unknown form"
                    }
                },
                { 
                    Idioma.Italiano, 
                    new Mensaje() {
                        ListaVacia = "<h1>Lista vuota di forme!</h1>",
                        TituloReporte = "<h1>Rapporto delle forme</h1>",
                        nombresForma = new Dictionary<Forma, string>
                                        {
                                            { Forma.Cuadrado, "Quadrati" },
                                            { Forma.Circulo, "Cerchi"},
                                            { Forma.TrianguloEquilatero, "Triangoli"},
                                            { Forma.Trapecio, "Trapezi" }
                                        },
                        sTotal =  "TOTALE:<br/>",
                        sForma = "forme",
                        sPerimetro = "Perimetro",
                        sArea = "Area",
                        sDesconocido = "Forma sconosciuta"
                    }
                },
        };
        #endregion

        #region Metodos
        public FormaGeometrica(Forma tipo, decimal ancho) 
        {
            Tipo = tipo;
            _lado = ancho;
        }
        public FormaGeometrica(Forma tipo, decimal baseMayor, decimal baseMenor, decimal altura)
        {
            Tipo = tipo;
            _baseMayor = baseMayor;
            _baseMenor = baseMenor;
            _altura = altura;
        }

        public static string Imprimir(List<FormaGeometrica> formas, Idioma idioma)
        {
            var sb = new StringBuilder();

            if (formas.Count == 0) {
                sb.Append(mensajes[idioma].ListaVacia);
            }
            else {
                sb.Append(mensajes[idioma].TituloReporte);
                var reporte = ObtenerFormas(formas, idioma);
                sb.Append(reporte);
            }

            return sb.ToString();
        }

        private static string ObtenerFormas(List<FormaGeometrica> formas, Idioma idioma)
        {
            var sb = new StringBuilder();

            foreach (var forma in mensajes[idioma].nombresForma)
            {
                var formasFiltradas = formas.FindAll(f => f.Tipo == forma.Key);
                if (formasFiltradas.Count > 0)
                {
                    sb.Append($"{formasFiltradas.Count} {forma.Value} | ");
                    sb.Append($"{mensajes[idioma].sArea} {formasFiltradas.Sum(f => f.CalcularArea(idioma)):#.##} | ");
                    sb.Append($"{mensajes[idioma].sPerimetro} {formasFiltradas.Sum(f => f.CalcularPerimetro(idioma)):#.##} <br/>");
                }
            }

            sb.Append($"{mensajes[idioma].sTotal}{formas.Count} {mensajes[idioma].sForma} ");
            sb.Append($"{mensajes[idioma].sPerimetro} {formas.Sum(f => f.CalcularPerimetro(idioma)):#.##} ");
            sb.Append($"{mensajes[idioma].sArea} {formas.Sum(f => f.CalcularArea(idioma)):#.##}");

            return sb.ToString();
        }

        public virtual decimal CalcularArea(Idioma idioma)
        {
            switch (Tipo)
            {
                case Forma.Cuadrado: return _lado * _lado;
                case Forma.Circulo: return (decimal)Math.PI * (_lado / 2) * (_lado / 2);
                case Forma.TrianguloEquilatero: return ((decimal)Math.Sqrt(3) / 4) * _lado * _lado;
                case Forma.Trapecio: return ((_baseMayor + _baseMenor) * _altura) / 2;
                default:
                    throw new ArgumentOutOfRangeException(mensajes[idioma].sDesconocido);
            }
        }

        public virtual decimal CalcularPerimetro(Idioma idioma)
        {
            switch (Tipo)
            {
                case Forma.Cuadrado: return _lado * 4;
                case Forma.Circulo: return (decimal)Math.PI * _lado;
                case Forma.TrianguloEquilatero: return _lado * 3;
                case Forma.Trapecio: return _lado * 2 + _baseMayor + _baseMenor;
                default:
                    throw new ArgumentOutOfRangeException(mensajes[idioma].sDesconocido);
            }
        }
    }
    #endregion
}
