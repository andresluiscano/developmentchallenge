﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentChallenge.Data.Classes
{
    public class Mensaje
    {
        public string ListaVacia { get; set; }
        public string TituloReporte { get; set; }
        public string Nombre { get; set; }
        public Dictionary<Forma, string> nombresForma { get; set; }
        public string sTotal { get; set; }
        public string sForma { get; set; }
        public string sPerimetro { get; set; }
        public string sArea { get; set; }
        public string sDesconocido { get; set; }
    }
}
